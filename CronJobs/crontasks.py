#!/usr/bin/env python

""" Secure lora - crontask """

__author__ = "Jobin J"
__copyright__ = ""
__credits__ = [""]
__license__ = ""
__version__ = "1.0.1"
__maintainer__ = "Jobin J"
__email__ = ""
__status__ = "Development"


import requests
from datetime import date
import json
from influxdb import InfluxDBClient
import threading
import datetime
import base64

today = date.today()

run_thread = []

# 3 minutes interval for checking flooding
flood_time_interval = 3
# 30 minutes interval for checking JRR
jr_time_interval = 30
# 12 hour interval for checking packet lose
plose_time_interval = 12*60

# LORASERVER -  credentials & urls
email_list = ["test@icfoss.org"]
password_list = ["123454321"]
LORASERVERURL_LIST=["https://loraserver.icfoss.org"]
GATEWAYS = []

# InfluxDb database credentials
db_client = InfluxDBClient(host='183.143.23.193', port=8086,username='techworldthink', password='1234')
db_client.switch_database('secure_lora')
db_client_gw = InfluxDBClient(host='183.143.23.193', port=8086,username='techworldthink', password='1234')
db_client_gw.switch_database('gwdb')

# get gateway status (True / False)
def gateway_status(measurement_name):
    data = db_client_gw.query("SELECT * FROM " + measurement_name + " ORDER BY time DESC LIMIT 1")
    result = list(data.get_points(measurement=measurement_name))
    try:
        last_seen =  result[0]['time']
        last_seen = string_to_timedate(last_seen)
        crnt_time = string_to_timedate(get_crnt_timedate())
        seconds = (crnt_time - last_seen).total_seconds()
        hours = seconds / (60*60)
        if hours <= plose_time_interval:
            return True 
        return False
    except Exception as e:
        print(e)
        return False

# get previous datetime with format (yyyy-MM-ddTHH:mm:ss.fZ)
def get_prev_timedate(time_interval):
    crnt_time = datetime.datetime.now(datetime.timezone.utc)
    compare_time = crnt_time - datetime.timedelta(minutes = time_interval)
    formatted_date = compare_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    return formatted_date

# string datetime to datetime object
def string_to_timedate(datetime_str):
    return datetime.datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ")

# get current datetime with format (yyyy-MM-ddTHH:mm:ss.fZ)
def get_crnt_timedate():
    crnt_time = datetime.datetime.now(datetime.timezone.utc)
    formatted_date = crnt_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    return formatted_date

def get_lastcheck_datetime():
    now = datetime.datetime.now()
    return now.strftime("%d/%m/%Y %H:%M:%S")

# check the join request already exist or not
def is_joinrequest_exist(device_eui,crnt_datetime):
    data = db_client.query("SELECT * FROM \"securelora_joinrequest\" WHERE devEUI='" + device_eui +"' and time > '"+ crnt_datetime +"'  ORDER BY time DESC LIMIT 1")
    result = list(data.get_points(measurement='securelora_joinrequest'))
    if len(result) < 1:
        return 0
    return 1

# check the join request already exist or not
def is_devnonce_exist(device_eui,dev_nonce):
    data = db_client.query("SELECT * FROM \"securelora_joinrequest\" WHERE devEUI='" + device_eui +"' and devNonce = '"+ str(dev_nonce) +"'  ORDER BY time DESC")
    result = list(data.get_points(measurement='securelora_joinrequest'))
    if len(result) < 1:
        return 0
    return 1

# check the join request already exist or not
def check_flooding_status(device_addr,prev_time):
    data = db_client.query("SELECT * FROM \"securelora_up\" WHERE devAddr='" + device_addr +"' and time >='"+ prev_time +"' ")
    result = list(data.get_points(measurement='securelora_up'))
    if len(result) > 4 : 
        return True 
    return False

# get packet lose status
def get_packetloss_status(device_addr,prev_time,gateway_id):
    if(not gateway_status(gateway_id)):
        return False
    data = db_client.query("SELECT * FROM \"securelora_up\" WHERE devAddr='" + device_addr +"' and time >='"+ prev_time +"' ")
    result = list(data.get_points(measurement='securelora_up'))
    if len(result) > 0:
        first_frame_count = result[0]['fCnt']
        last_frame_count = result[-1]['fCnt']
        count_diff =  last_frame_count - first_frame_count
        if count_diff > len(result):
            return True
    return False

# delete existing data of a device (for overwriting a record)
def delete_jr_device_record(measurement_name,device_eui,type):
    data = db_client.query("DELETE FROM " + measurement_name + " WHERE devEUI ='" + device_eui +"' and type ='" + type + "'")
    result = list(data.get_points(measurement=measurement_name))

# delete existing data of a device (for overwriting a record)
def delete_up_device_record(measurement_name,device_addr,type):
    data = db_client.query("DELETE FROM " + measurement_name + " WHERE devAddr ='" + device_addr +"' and type ='" + type + "'")
    result = list(data.get_points(measurement=measurement_name))

# get jwt token
def apilogin(email,password,url):
  url=url+'/api/internal/login'
  myobj = '{"password": "'+password+'","email": "'+email+'"}'
  headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
  x = requests.post(url, data = myobj, headers=headers)
  data = x.json()
  return data['jwt']

# check frame type
def get_frame_type(json_data):
    if "result" in json_data:
       
        json_data = json_data['result'] 
        if "uplinkFrame" in json_data:
            json_data = json_data['uplinkFrame'] 
        if "downlinkFrame" in json_data:
            json_data = json_data['downlinkFrame']    
        if "phyPayloadJSON" in json_data:
            json_data = json_data['phyPayloadJSON']  
        if "mhdr" in json_data:
            temp = json.loads(json_data)
            if "mType" in temp["mhdr"]:
                return temp["mhdr"]["mType"]
    return ""


# return Join Request Data
def get_joinrequest_data(json_data):
    return get_up_data(json_data)
# return Unconfirmed Data
def get_unconfup_data(json_data):
    return get_up_data(json_data)
# return Confirmed Data
def get_confup_data(json_data):
    return get_up_data(json_data)


# check the frame contain -  Unconfirmed up / Confirmed up / Join request data 
# if found return data else null
def get_up_data(json_data):
    response = [] 
    if "result" in json_data:
        json_data = json_data['result'] 
        if "uplinkFrame" in json_data:
            json_data = json_data['uplinkFrame'] 
            if "phyPayloadJSON" in json_data:
                frequency = json_data['txInfo']['frequency']
                spreading_factor = json_data['txInfo']['loRaModulationInfo']['spreadingFactor']
                code_rate = json_data['txInfo']['loRaModulationInfo']['codeRate']
                rssi = json_data['rxInfo'][0]['rssi']
                gateway_id = base64.b64decode(json_data['rxInfo'][0]['gatewayID']).hex()
                json_data = json_data['phyPayloadJSON']  
                if "mhdr" in json_data:
                    temp = json.loads(json_data)
                    if "mType" in temp["mhdr"]:
                        if temp["mhdr"]["mType"] == "ConfirmedDataUp" or temp["mhdr"]["mType"] == "UnconfirmedDataUp":
                            response.append(frequency)
                            response.append(spreading_factor)
                            response.append(code_rate)
                            response.append(rssi)
                            response.append(temp['macPayload']['fhdr']['devAddr'])
                            response.append(temp['macPayload']['fhdr']['fCnt'])
                            response.append(temp["mhdr"]["mType"]) 
                            response.append(temp['mic'])
                            response.append(gateway_id)
                            return response
                        elif temp["mhdr"]["mType"] == "JoinRequest":
                            response.append(frequency)
                            response.append(spreading_factor)
                            response.append(code_rate)
                            response.append(rssi)
                            response.append(temp['macPayload']['joinEUI'])
                            response.append(temp['macPayload']['devEUI']) 
                            response.append(temp['mic'])
                            response.append(temp['macPayload']['devNonce'])
                            response.append(gateway_id)
                            return response
                        else:
                            return None

# Add join request data to database
def save_joinrequest_data(data):
    print("-jr/dev-")
    # to check the join request fake or not
    jr_status = is_joinrequest_exist(data[5],get_prev_timedate(jr_time_interval))
    dev_nonce_status = is_devnonce_exist(data[5],data[7])
    json_body = [{
        "measurement": "securelora_joinrequest",
		"tags": {"devEUI" : data[5]},
		"fields":{
            "frequency"         : data[0],
            "spreading_factor"  : data[1],
            "code_rate"         : data[2],
            "rssi"              : data[3],
            "joinEUI"           : data[4],
            #"devEUI"            : data[5],
            "mic"               : data[6], 
            "devNonce"          : data[7],
            "gatewayID"         : data[8],
            "jr_status"         : jr_status,
            "dev_nonce_status"  : dev_nonce_status
        }
    }]
    
    #print(json_body)
    db_client.write_points(json_body)  

    # write status of join request replay / dev nonce repeate
    if(jr_status == 1):
        delete_jr_device_record("securelora_jrr_dns_status",data[5],"JRR")
        json_body = [{
            "measurement": "securelora_jrr_dns_status",
            "tags": {"devEUI" : data[5],"type":"JRR","gatewayID" : data[8]},
            "fields":{
                "last_check"        : get_lastcheck_datetime(),
            }
        }]
        db_client.write_points(json_body)  

       
    if(dev_nonce_status == 1):
        delete_jr_device_record("securelora_jrr_dns_status",data[5],"DNS")
        json_body = [{
            "measurement": "securelora_jrr_dns_status",
            "tags": {"devEUI" : data[5],"type":"DNS","gatewayID" : data[8]},
            "fields":{
                "last_check"        : get_lastcheck_datetime(),
            }
        }]
        db_client.write_points(json_body) 

# Add Confirmed/Unconfirmed up data to database
def save_up_data(data):
    print("-up & f/p-")
    # to check the join request fake or not
    json_body = [{
        "measurement": "securelora_up",
		"tags": {"devAddr" : data[4]},
		"fields":{
            "frequency"         : data[0],
            "spreading_factor"  : data[1],
            "code_rate"         : data[2],
            "rssi"              : data[3],
            #"devAddr"          : data[4],
            "fCnt"              : data[5],
            "mType"             : data[6],
            "mic"               : data[7], 
            "gatewayID"         : data[8]
        }
    }]
    db_client.write_points(json_body)  

    if(data[5] == 0):
        delete_up_device_record("securelora_flood_plose_status",data[4],"RST")
        json_body = [{
            "measurement": "securelora_flood_plose_status",
            "tags": {"devAddr" : data[4],"type":"RST","gatewayID" : data[8]},
            "fields":{
                "last_check"        : get_lastcheck_datetime(),
            }
        }]
        db_client.write_points(json_body) 

    if(check_flooding_status(data[4],get_prev_timedate(flood_time_interval))):
        delete_up_device_record("securelora_flood_plose_status",data[4],"FLOOD")
        json_body = [{
            "measurement": "securelora_flood_plose_status",
            "tags": {"devAddr" : data[4],"type":"FLOOD","gatewayID" : data[8]},
            "fields":{
                "last_check"        : get_lastcheck_datetime(),
            }
        }]
        db_client.write_points(json_body) 

    if(get_packetloss_status(data[4],get_prev_timedate(plose_time_interval),data[8])):
        delete_up_device_record("securelora_flood_plose_status",data[4],"PLOSE")
        json_body = [{
            "measurement": "securelora_flood_plose_status",
            "tags": {"devAddr" : data[4],"type":"PLOSE","gatewayID" : data[8]},
            "fields":{
                "last_check"        : get_lastcheck_datetime(),
            }
        }]
        db_client.write_points(json_body) 

def monitor_thread(gateway_id,url,headers,CREDENTIALS):
    print("-- STATUS - START - thread - gateway " + gateway_id)
    response = requests.get(url+"/api/gateways/"+gateway_id+"/frames", headers=headers, stream=True)

    # If token expires
    if(response.status_code == 401):
        LORASERVERAP=apilogin(CREDENTIALS[0],CREDENTIALS[1],CREDENTIALS[2])
        headers = { "Accept": "application/json","Grpc-Metadata-Authorization": "Bearer "+LORASERVERAP}
        monitor_thread(gateway_id,url,headers,CREDENTIALS)

    for data in response.iter_content(chunk_size=None):
        string_data = data.decode('utf-8')
        json_data = json.loads(string_data)
        frame_type = get_frame_type(json_data)

        if(frame_type == "JoinRequest"):
            data = get_joinrequest_data(json_data)
            save_joinrequest_data(data)
        elif(frame_type == "UnconfirmedDataUp"):
            data = get_unconfup_data(json_data)
            save_up_data(data)
        elif(frame_type == "ConfirmedDataUp"):
            data = get_confup_data(json_data)
            save_up_data(data)
        else:
            data = []
    print("-- STATUS - END - thread - gateway " + gateway_id)

# get frames and check for join requests
def monitor_and_save_frames(url,LORASERVERAP,CREDENTIALS):
    print("--START check and save frames")
    headers = { "Accept": "application/json","Grpc-Metadata-Authorization": "Bearer "+LORASERVERAP}
    # thread code add here....
    for gateway_id in GATEWAYS:
        print(" - Thread  -Gateway : " + gateway_id)
        run_thread.append(threading.Thread(target=monitor_thread, args=(gateway_id,url,headers,CREDENTIALS,)))
    print("--END check and save frames")


def get_gateways(url,LORASERVERAP):
    print("--START - get Gateway list")
    headers_dict = { "Accept": "application/json","Grpc-Metadata-Authorization": "Bearer "+LORASERVERAP}
    response = requests.get(url+"/api/gateways?limit=1000", headers=headers_dict,)
    data = response.json()
    data = data["result"]
    gateways = [gateway["id"] for gateway in data] 
    print("--END - get Gateway list")
    return gateways     

def threads_close():
    while(1):
        for each in run_thread:
            each.join()
        print("close all threads")

def main():
    global GATEWAYS               
    while(1):
        for x in range(len(LORASERVERURL_LIST)):
            print("--START URL : " + LORASERVERURL_LIST[x])
            LORASERVERURL = LORASERVERURL_LIST[x]
            email=email_list[x]
            password=password_list[x]
            LORASERVERAP=apilogin(email,password,LORASERVERURL)
            GATEWAYS = get_gateways(LORASERVERURL,LORASERVERAP)
            print("Gateways : ->")
            print(GATEWAYS)
            print("--START - monitoring")
            monitor_and_save_frames(LORASERVERURL,LORASERVERAP,[email,password,LORASERVERURL])
            print("--END URL : " + LORASERVERURL_LIST[x])  
            print("\n\n\n")
            print("Running ...... ")
            print("\n\n\n")

      
        for each in run_thread:
            each.start()

        # token_regenerate_thread = threading.Thread(target=threads_close, args=())
        # token_regenerate_thread.start()

        for each in run_thread:
            each.join()
        
        print("\n\n------------------------ Restart (API - regenerate) ----------------------------------\n\n")
        
        


if __name__ == "__main__":
    main()