from django.db import models
from datetime import datetime

class DeviceData(models.Model):
    dev_eui = models.CharField(max_length=128)
    last_seen_at = models.CharField(max_length=128,null=True)
    dev_addr = models.CharField(max_length=50)
    monitor_status = models.IntegerField(default=1)
    reg_date = models.DateTimeField(default=datetime.now,blank=True)
    updated_date = models.DateTimeField(default=datetime.now,blank=True) 
    