from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name="userhome"),
    path('dashboard',views.dashboard,name="dashboard"),
    path('device-manage',views.device_manage,name="device_manage"),
    path('toggle-device-status',views.device_status_toggle,name="device_status_toggle"),
    path('update-device-lastseen',views.update_device_lastseen,name="update_device_lastseen")
]

