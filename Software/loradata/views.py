from django.shortcuts import render
from . import dbconnector
from .utils  import time_formats
from . import device_info
from devicemanage import models as device_models


def get_deui_by_daddr(device_addr):
    try:
        print(device_addr)
        deviceObj = device_models.DeviceData.objects.get(dev_addr = device_addr)
        print(deviceObj.dev_eui)
        return deviceObj.dev_eui
    except Exception as e:
        print(e)
        return device_addr

def get_device_risk_status():
    response = []
    risk_counts = []
    LOW = 0
    MEDIUM = 0
    HIGH = 0
    jr_data = dbconnector.get_jr_dns_status()
    for data in jr_data:
        if data['type'] == "JRR":
            row = {
                "risk" : "HIGH",
                "description" : "Join Request replay",
                "date"         : time_formats.format_stringdate(data['time']),
                "last_check"   : data['last_check'],
                "devEUI"       : data['devEUI'],
                "gateway"      : data['gatewayID']
            }
            HIGH = HIGH + 1
            response.append(row)
        if data['type'] == "DNS":
            row = {
                "risk" : "LOW",
                "description" : "DevNonce reuse",
                "date"         : time_formats.format_stringdate(data['time']),
                "last_check"   : data['last_check'],
                "devEUI"       : data['devEUI'],
                "gateway"      : data['gatewayID']
            }
            LOW = LOW + 1
            response.append(row)

    jr_data = dbconnector.get_flood_plose_status()
    for data in jr_data:
        if data['type'] == "FLOOD":
            row = {
                "risk" : "MEDIUM",
                "description" : "Packet flooding",
                "date"         : time_formats.format_stringdate(data['time']),
                "last_check"   : data['last_check'],
                "devEUI"       : get_deui_by_daddr(data['devAddr']),
                "gateway"      : data['gatewayID']
            }
            MEDIUM = MEDIUM + 1
            response.append(row)
        if data['type'] == "PLOSE":
            row = {
                "risk" : "HIGH",
                "description" : "packet loss",
                "date"         : time_formats.format_stringdate(data['time']),
                "last_check"   : data['last_check'],
                "devEUI"       : get_deui_by_daddr(data['devAddr']),
                "gateway"      : data['gatewayID']
            }
            HIGH = HIGH + 1
            response.append(row)
        if data['type'] == "RST":
            row = {
                "risk" : "MEDIUM",
                "description" : "Device reset",
                "date"         : time_formats.format_stringdate(data['time']),
                "last_check"   : data['last_check'],
                "devEUI"       : get_deui_by_daddr(data['devAddr']),
                "gateway"      : data['gatewayID']
            }
            MEDIUM = MEDIUM + 1
            response.append(row)

    risk_counts.append(LOW)
    risk_counts.append(MEDIUM)
    risk_counts.append(HIGH)

    return [response,risk_counts]


def get_device_info():
    return device_info.get_device_info()

