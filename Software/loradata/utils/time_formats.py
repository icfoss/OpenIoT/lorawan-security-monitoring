import datetime 

def format_stringdate(datetime_str):
    date_obj =  datetime.datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ")
    formated_date = date_obj.strftime("%d/%m/%Y %H:%M:%S")
    return formated_date
