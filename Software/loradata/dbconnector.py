from influxdb import InfluxDBClient

# InfluxDb database credentials
db_client = InfluxDBClient(host='183.143.23.193', port=8086,username='techworldthink', password='1234')
db_client.switch_database('secure_lora')


def get_jr_dns_status():
    data = db_client.query("SELECT * FROM \"securelora_jrr_dns_status\" ")
    data = list(data.get_points(measurement='securelora_jrr_dns_status'))
    return data

def get_flood_plose_status():
    data = db_client.query("SELECT * FROM \"securelora_flood_plose_status\" ")
    data = list(data.get_points(measurement='securelora_flood_plose_status'))
    return data